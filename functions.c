#include <stdio.h>
#include <stdlib.h>

double mat_mul(double **A, double **B, double **C, int l, int m, int n)
{
	printf("================================ Matrix C ================================\n");
	for (int i=0; i<l; i++)
	{
		for (int j=0; j<n; j++)
		{
			for (int k=0; k<m; k++)
			{
				C[i][j] += A[i][k] * B[k][j];
			}
			printf("%lf\t", C[i][j]);
		}
		printf("\n");
	}
	printf("got here");
	return 0.0;
}

double **** split_matrix(double ** A, int nrow, int ncol, int num_blocks)
{ 
	double **** sub_block = malloc(nrow/num_blocks * sizeof(double ***));
	for (int i=0; i<num_blocks; i++)
	{
		sub_block[i] = malloc(num_blocks * sizeof(double **));
		for (int j=0; j<num_blocks; j++)
		{
			sub_block[i][j] = malloc(ncol/num_blocks * sizeof(double *));
			for (int k=0; k<ncol/num_blocks; k++)
				sub_block[i][j][k] = malloc(ncol/num_blocks * sizeof(double));
		}
	}
	for (int k=0; k<num_blocks; k++)	
	{
		for (int l=0; l<num_blocks; l++)
		{
			for (int i=0; i<nrow/num_blocks; i++)	
			{
				for (int j=0; j<ncol/num_blocks; j++)
					sub_block[k][l][i][j] = A[(nrow/num_blocks)*k + i][(ncol/num_blocks)*l + j];
			}
		}
	}
	return sub_block;
}
