#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <mpi.h>

#include "functions.h"

int main(int argc, char * argv[])
{
	//MPI_Init(&argc, &argv);
	int l, m, n, num_blocks;

	//default to square matrix of size n=4 split into 4 different blocks if no args given
	if (argc == 1)
	{
		l = 9;
		m = 9;
		n = 9;
		num_blocks = sqrt(l);
	}
	
	else
	{
		//get matrix dimensions from command line input
		l = atoi(argv[1]);
		m = atoi(argv[2]);
		n = atoi(argv[3]);
		num_blocks = atoi(argv[4]);
		if (l % num_blocks != 0)
		{
			printf("matrix not evenly divisble into blocks\n");
			return 0;
		}
		if (m % num_blocks != 0)
		{
			printf("matrix not evenly divisble into blocks\n");
			return 0;
		}
		if (n % num_blocks != 0)
		{
			printf("matrix not evenly divisble into blocks\n");
			return 0;
		}
	}

	double **A = malloc(l * sizeof(double *));
	double **B = malloc(m * sizeof(double *));
	double **C = malloc(l * sizeof(double *));
	for (int i=0; i<l; i++)
	{
		A[i] = malloc(m * sizeof(double));
		C[i] = malloc(n * sizeof(double));
	}	
	for (int i=0; i<m; i++)
		B[i] = malloc(n * sizeof(double));

	mat_init(A, B, l, m, n);  //allocate random values to A and B

	//multiply matrices and print C
	mat_mul(A, B, C, l, m, n);
	printf("got here");

	//create process grid
	double **proc_grid = malloc(l * sizeof(double *));
	for (int i=0; i<l; i++)
		proc_grid[i] = malloc(n * sizeof(double));

	printf("got here");
	double **** Ap, ****Bp, ****Cp;  //Ap for A partitioned (into blocks)
	Ap = split_matrix(A, l, m, num_blocks);
	Bp = split_matrix(B, m, n, num_blocks);
	Cp = split_matrix(C, l, n, num_blocks);
	
	/*
	printf("A_block[0][0]:\n");
	for (int i=0; i<l/num_blocks; i++)	
	{
		for (int j=0; j<m/num_blocks; j++)
			printf("%lf ", Ap[0][0][i][j]);
		printf("\n");
	}
	*/
	shiftA(Ap, num_blocks);
	shiftB(Bp, num_blocks);

	/*
	for (int k=0; k<num_blocks; k++)	
	{
		for (int h=0; h<num_blocks; h++)
		{
			mat_mul(Ap[k][h], Bp[k][h], Cp[k][h], (l/num_blocks)*k, (l/num_blocks)*k, (l/num_blocks)*h)  //need to save Cp and do this twice more after shifting again before printing C
		}
	} 

	*/
	for (int k=0; k<num_blocks; k++)	
	{
		for (int h=0; h<num_blocks; h++)
		{
			for (int i=0; i<l/num_blocks; i++)	
			{
				for (int j=0; j<n/num_blocks; j++)
				{
					for (int p=0; p<m/num_blocks; p++)
						Cp[k][h][i][j] += Ap[k][h][i][p] * Bp[k][h][p][j];
				}
			}
		}
	}
	
	for (int num_shifts=0; num_shifts<num_blocks-1; num_shifts++)
	{
		shift_left(Ap, num_blocks);
		shift_up(Bp, num_blocks);
		for (int k=0; k<num_blocks; k++)	
		{
			for (int h=0; h<num_blocks; h++)
			{
				for (int i=0; i<l/num_blocks; i++)	
				{
					for (int j=0; j<n/num_blocks; j++)
					{
						for (int p=0; p<m/num_blocks; p++)
							Cp[k][h][i][j] += Ap[k][h][i][p] * Bp[k][h][p][j];
					}
				}
			}
		}
	}
	printf("----------------C after cannon--------------------------------");
	for (int k=0; k<num_blocks; k++)	
	{
		for (int h=0; h<num_blocks; h++)
		{
			for (int i=0; i<l/num_blocks; i++)	
			{
				for (int j=0; j<n/num_blocks; j++)
					printf("lf ", Cp[k][h][i][j]);
			printf("\n");
		}
	}
	}
	/*
	for (int i=0; i<l/num_blocks; i++)	
	{
		for (int j=0; j<n/num_blocks; j++)
			mat_mul()
	*/

	free(A);
	free(B);
	free(C);
	free(Ap);
	free(Bp);
	free(Cp);
	//MPI_Finalize();
	return 0;
}
