main: functions.o mat_init.o shift.o functions.h
	gcc -Wall -o main main.c functions.o mat_init.o shift.o -std=c99 -lm
functions.o: functions.c
	gcc -c functions.c -std=c99
mat_init.o: mat_init.c
	gcc -c mat_init.c -std=c99
shift.o: shift.c
	gcc -c shift.c -std=c99
clean:
	rm *.o main
