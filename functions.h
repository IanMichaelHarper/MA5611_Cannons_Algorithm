double mat_mul(double **A, double **B, double **C, int l, int m, int n);
double ** split_matrix(double ** A, int nrow, int ncol, int num_blocks);
void mat_init(double **A, double ** B, int l, int m, int n);
double shiftA(double **** A, int num_blocks);
double shiftB(double **** B, int num_blocks);
void shift_left(double **** A, int num_blocks);
void shift_up(double **** B, int num_blocks);
