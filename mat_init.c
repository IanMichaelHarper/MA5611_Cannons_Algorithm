#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void mat_init(double **A, double ** B, int l, int m, int n)
{
	

	//fill A with random values and print
	srand(time(NULL));
	printf("============================ Matrix A ============================\n");
	for (int i=0; i<l; i++)	
	{
		for (int j=0; j<m; j++)
		{
			double random = rand();
			A[i][j] = (random/RAND_MAX)*4-2;
			//A[i][j] = (rand() * pow(-1.0, rand())) % mod;
			printf("%lf\t", A[i][j]);
		}
		printf("\n");
	}

	//fill B with random values and print
	printf("================================ Matrix B ================================\n");
	for (int j=0; j<m; j++)
	{
		for (int k=0; k<n; k++)
		{
			double random = rand();
			B[j][k] = (random/RAND_MAX)*4-2;
			printf("%lf\t", B[j][k]);
		}
		printf("\n");
		
	}
}
