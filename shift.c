void shiftA(double **** A, int num_blocks)
{
	int count = 0;
	int i = 0;
	while(i != num_blocks)
	{
		while (count != 0);
		{
			double ** temp = A[i][0];
			int j;
			for (j=0; j<num_blocks-1; j++)
				A[i][j] = A[i][j+1];
			A[i][num_blocks-1] = temp;
			count--;
		}
		i++;
		count = i;
	}
}

void shiftB(double **** B, int num_blocks)
{
	int count = 0;
	int j = 0;
	while(j != num_blocks)
	{
		while (count != 0);
		{
			double ** temp = B[0][j];
			int i;
			for (i=0; i<num_blocks-1; i++)
				B[i][j] = B[i+1][j];
			B[num_blocks-1][j] = temp;
			count--;
		}
		j++;
		count = j;
	}
}

void shift_left(double **** A, int num_blocks)
{
	for (int i=0; i<num_blocks; i++)
	{
		double ** temp = A[i][0];
		int j;
		for (j=0; j<num_blocks-1; j++)
			A[i][j] = A[i][j+1];
		A[i][num_blocks-1] = temp;
	}
}

void shift_up(double **** B, int num_blocks)
{
	for (int j=0; j<num_blocks; j++)
	{
		double ** temp = B[0][j];
		int i;
		for (i=0; i<num_blocks-1; i++)
			B[i][j] = B[i+1][j];
		B[num_blocks-1][j] = temp;
	}
}
